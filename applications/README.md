# applications

This directory contains `.desktop` files, which essentially define custom user applications. These files contain information like an application's title, icon, command, whether it requires a terminal, etc.


Program launchers like `rofi`, `dmenu-run`, or even `gnome`'s application list should update to include any new applications you place in this directory, making them very easy to run.
