# sxhkd

`sxhkd` is a Simple X Hotkey Keyboard Daemon that can be run over any window manager, however it is most commonly used with `bspwm`. It's extremely lightweight and simple to configure.

# installation

From the `Debian` repository:
```bash
sudo apt install sxhkd
```

# configuration

Configuration is done via `~/.config/sxhkd/sxhkdrc`.

# useful information

Let's face it: try reading the `sxhkd` man page, or doing some sxhkd research, and you'll find it's all quite poorly documented. Here's some useful information I've gathered.

Sources:
* [ArchWiki forum thread](https://bbs.archlinux.org/viewtopic.php?id=155613)

## hotkey modes

Some may be familiar with i3wm's hotkey "modes." Essentially, it means that you can enter a mode via a hotkey, and once inside the "mode", hotkeys can be redefined. For example:

```
super + r: {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}
```

So, to resize a window, first press `super + r`. Then you can let go of those keys and just press h, j, k, l and the window is resized. To exit a mode and return to your normal keybindings, hit `escape`.

## multi-line commands

Let's say you want a hotkey to execute a block of bash code. It's possible to configure a hotkey to simply execute a separate bash script like so:

```
super + v
	bash ~/bin/set_bspwm_padding.sh
```

However, if your script is only a few lines, the same thing can be accomplished like this:

```
super + v
	bspc config top_padding 10; \
	bspc config bottom_padding 10; \
	bspc config left_padding 10; \
	bspc config right_padding 10
```

As you can see, each line (except for the last one) ends with `; \` which basically says, hey, on the next line is another command. Notice that you *cannot* leave empty lines between commands when using this method.

## multiple, similar hotkeys in one code block

Instead of writing four separate hotkeys:

```
super + h
    bspc focus left

super + j
    bspc focus down

super + k
    bspc focus up

super + l
    bspc focus right
```

You can instead shorten these to one hotkey definition:

```
super + {h,j,k,l}
    bspc focus {left,down,up,right}
```
