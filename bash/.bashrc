# ~/.bashrc: the bash "runcom", or run control; runs upon bash startup

shopt -s histappend	# append to history, don't overwrite
HISTSIZE=5000		# max commands in HISTFILE
HISTFILESIZE=5000	# max lines in HISTFILE
HISTCONTROL=erasedups	# remove duplicate lines in HISTFILE

set -o vi		# for vi-like editing in terminal

# color prompt based on exit code
BOLD="\[$(tput bold)\]"
# RED="\[$(tput setaf 1)\]"
# GREEN="\[$(tput setaf 2)\]"
# ORANGE="\[$(tput setaf 3)\]"
# CYAN="\[$(tput setaf 4)\]"
# PURPLE="\[$(tput setaf 5)\]"
# WHITE="\[$(tput setaf 7)\]"
DARK_RED="\[$(tput setaf 9)\]"
DARK_GREEN="\[$(tput setaf 10)\]"
DARK_ORANGE="\[$(tput setaf 11)\]"
DARK_CYAN="\[$(tput setaf 12)\]"
DARK_PURPLE="\[$(tput setaf 13)\]"
DARK_WHITE="\[$(tput setaf 15)\]"
RESET="\[$(tput sgr0)\]"
__prompt_command() {
	returnValue="$?"
	[ "$returnValue" -ne 0 ] && PS1="${BOLD}${DARK_RED}[\u${DARK_GREEN}@${DARK_CYAN}\h ${DARK_PURPLE}\W${DARK_RED} $returnValue]${DARK_WHITE}$ ${RESET}" || PS1="${BOLD}${DARK_RED}[${DARK_ORANGE}\u${DARK_GREEN}@${DARK_CYAN}\h ${DARK_PURPLE}\W${DARK_RED}]${DARK_WHITE}$ ${RESET}"; \
}

case "$TERM" in
	xterm-color|alacritty|*-256color|foot) PROMPT_COMMAND=__prompt_command ;;
	*) PS1='[\u@\h:\w]\$ ' ;;
esac

[ -f ~/.bash_aliases ] && . ~/.bash_aliases	# source .bash_aliases

xrdb ~/.Xresources	# refresh x resources database

# color man pages when using less
export LESS_TERMCAP_md=$'\e[1;31m'	# begin bold (red)
export LESS_TERMCAP_so="$(tput setaf 0)$(tput setab 11)"	# standout mode
export LESS_TERMCAP_us=$'\e[1;32m'	# begin underline (green)
export LESS_TERMCAP_me=$'\e[0m'		# reset bold/blink
export LESS_TERMCAP_se=$'\e[0m'		# reset reverse video
export LESS_TERMCAP_ue=$'\e[0m'		# reset underline

# fix blank screen in some java applications
export _JAVA_AWT_WM_NONREPARENTING=1

# sets defaults for applications
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_PICTURES_DIR="$HOME/Pictures"
export VISUAL="nvim"
export EDITOR="$VISUAL"
export BROWSER="firefox"
export READER="zathura"

export PATH="$PATH:/home/john/.local/bin"
command -v cbonsai >/dev/null && sleep 0.1 && cbonsai -p

# source custom bash completions
. /etc/bash_completion
