file="$1"

if [ -z "$file" ]; then
	echo "error: file must be specified"
	exit 1
fi

base="$(basename "$file")"

outputDir=~/Pictures/"${base%.*}"
mkdir -p "$outputDir"
while true; do
	read -rp "timestamp: " timestamp
	read -rp "     name: " name
	[ -z "$timestamp" ] && timestamp="$(cat /tmp/vlc-timestamp)"

	ffmpeg -ss "$timestamp" -i "$file" -v warning -vframes 1 "$outputDir/${name}.png"
	echo "$timestamp -- $name" >> "$outputDir/timestamps.txt"
	echo
done
