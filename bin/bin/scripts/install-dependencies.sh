#!/usr/bin/env bash

# this script installs dependencies for my dotfiles from different places

pkgs_apt=(\
	# WM
	redshift\
	unclutter\

	# GUI programs
	thunderbird\
	dolphin\
	gparted\
	libreoffice\
	blueman\
	pavucontrol\
	zathura\
	sxiv\
	vlc\
	beets\

	## terminal programs
	# basic
	sudo\
	make automake\
	git\
	stow\
	cmake\
	bc\
	htop\
	curl\
	
	# productive
	tmux\
	pandoc\
	colordiff\
	ffmpeg\
)

apt install "${pkgs_apt[@]}"
