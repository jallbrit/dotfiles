#!/usr/bin/env bash

show_help() {
	echo "
Usage: 4ker [OPTIONS] SOURCE [DESTINATION]

Convert SOURCE image(s) to 4K and place them at DESTINATION.
SOURCE may be either a file or folder.

Options:
  -v, --verbose   increase verbosity
  -h, --help      display this help and exit
"
}

make_4k() {
	local file="$1"

	[ -f "$file" ] || return

	# skip if its not a png/jpg
	base="$(basename "$file")"
	case "${file##*.}" in
		jpg|jpeg|png) ;;
		*) return ;;
	esac

	## enhance resolution to at least 4k
	out="$dest/$base"

	# ensure destination file isn't already 4k
	out_width="$(identify -format "%w" "$out" 2>/dev/null)"
	out_height="$(identify -format "%h" "$out" 2>/dev/null)"

	if [ "$out_width $out_height" == "3840 2160" ]; then
		((verbose)) && echo "Skipping: destination image is already 4K"
		return
	else
		echo "Converting '$file' -> '$out'"
	fi


	width="$(identify -format "%w" "$file")"
	height="$(identify -format "%h" "$file")"
	((verbose)) && echo "Input resolution: $width x $height"

	# the biggest scale needs to happen
	scale="$(echo "3840 / $width" | bc -l)"
	scaleY="$(echo "2160 / $height" | bc -l)"
	(( $(echo "$scaleY > $scale" | bc -l) )) && scale="$scaleY"

	# upscale if needed
	if (( $(echo "$scale > 1" | bc -l) )); then
		((verbose)) && echo "Upscaling using scale: $scale"
		waifu2x-converter-cpp --log-level 0 --scale-ratio "$scale" -i "$file" -o "$out"
	else
		((verbose)) && echo "Scaling not necessary"
		cp "$file" "$out"
	fi

	## convert to 16:9 aspect ratio
	((verbose)) && echo "Cropping image to 16:9"
	mogrify -gravity center -crop 16:9 "$out"

	((verbose)) && echo
}

while true; do
	case "$1" in
		-h|--help)
			show_help
			exit
			;;

		-v|--verbose)
			verbose=$((verbose + 1))
			;;

		--)
			shift
			break
			;;

		-?*)
			echo "WARN: Unknown option (ignored): $1"
			;;

		*)
			break
	esac

	shift
done

src="$1"
[ -d "$src" ] && src="$(readlink -f "$src")" && src="${src%/}"

dest="${2:-4k}"
dest="${dest%/}"

# ensure user wants to continue
echo "Converting '$src' to 4k"
echo "Placing at: $dest"
read -rp "Ok? [y/N]: "
[ "$REPLY" != 'y' ] && exit 0

mkdir -p "$dest"

if [ -f "$src" ]; then
	make_4k "$src"

elif [ -d "$src" ]; then
	for file in "$src"/*; do
		make_4k "$file"
	done
fi
