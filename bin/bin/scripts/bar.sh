#!/usr/bin/env bash

# -- vars --
width=20

sepLeft="["
sepRight="]"

charFill="#"
charLine="#"
#charLine="─"
charEmpty="."

showSides=1

message=""
valueLeft=""
valueRight=""

OPTS="hw:a:b:l:r:sm:F:E:L:"
LONGOPTS="help,width,leftsep,rightsep,leftvalue,rightvalue,sides,message,fill,empty,line"

# getopt using those options; parse through "$@"
parsed=$(getopt --options=$OPTS --longoptions=$LONGOPTS -- "$@")
eval set -- "${parsed[@]}"

while true; do
	case "$1" in
		-h|--help)
			helpMe=1
			shift
			;;

		-w|--width)
			width=$2
			shift 2
			;;

		-a|--leftsep)
			sepLeft="$2"
			shift 2
			;;

		-b|--rightsep)
			sepRight="$2"
			shift 2
			;;

		-l|--leftvalue)
			valueLeft="$2"
			shift 2
			;;

		-r|--rightvalue)
			valueRight="$2"
			shift 2
			;;

		-s|--sides)
			showSides=0
			shift
			;;

		-m|--message)
			message="$2"
			shift 2
			;;

		-F|--fill)
			charFill="$2"
			shift 2
			;;

		-E|--empty)
			charEmpty="$2"
			shift 2
			;;

		-L|--line)
			charLine="$2"
			shift 2
			;;

		--) # end of arguments
			shift
			break
			;;

		*)
			echo "error while parsing arguments"
			exit 1
			;;
	esac
done

HELP="\
Usage: bar.sh [OPTIONS] CURRENT_VALUE TOTAL_VALUE

- values can be integers, floats, or times.

Optional args:
  -w,--width INT      define bar width
  -a,--leftsep STR    left boundary string
  -b,--rightsep STR   right boundary string
  -s,--sides          don't show side values
  -l,--leftvalue STR  value left of bar
  -r,--rightvalue STR value right of bar
  -F,--fill           character used to fill bar
  -E,--empty          character used to represent empty space
  -L,--line           character used to represent present location
  -m,--message        message to print before bar
  -h,--help           show help"

((helpMe)) && echo "$HELP" && exit 0

current="$1"
total="$2"

[ "$valueLeft" = "" ] && valueLeft="$current"
[ "$valueRight" = "" ] && valueRight="$total"

# convert dates into integers
[[ "$current" =~ ":" ]] && current=$(echo "$current" | awk -F : '{print ($1 * 60) + $2}')
[[ "$total" =~ ":" ]] && total=$(echo "$total" | awk -F : '{print ($1 * 60) + $2}')

# use awk to do math, get blocks and blanks
blocks=$(echo "$current $total $width" | awk '{print int( $1 / $2 * $3 )}')
blanks=$(( width - blocks - 1 ))

# create bar
bar=""
for (( i=1; i<=(blocks - 1); i++ )); do
	bar+="$charFill"
done
bar+="$charLine"
for (( i=1; i<=blanks; i++ )); do
	bar+="$charEmpty"
done

# ensure bar isn't too big
[ "${#bar}" -ge "$width" ] && bar=${bar:0:$((width-1))}

# create output
output="$message"
case "$showSides" in
	0) output+="$sepLeft$bar$sepRight" ;;
	1) output+="$valueLeft $sepLeft$bar$sepRight $valueRight" ;;
esac

echo "$output"
