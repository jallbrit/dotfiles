#!/usr/bin/env bash

# this script runs a command as many times as you'd like and averages the time it takes to run that command to give a good estimate.
# usage: timeavg ITERATIONS COMMAND

iters="$1"
cmd="$2"
echo -e "iters:\t\t$iters\ncommand:\t$2"
echo -ne "avg:\t\t"

sum=0
for (( i=0; i < iters; i++ )); do
	time=$( ( time -p $cmd &> /dev/null; ) 2>&1 | head -n 1 | awk '{print $2}' )
	sum=$(echo "$sum + $time" | bc -l)
done

echo "scale=3; $sum / $i" | bc -l
