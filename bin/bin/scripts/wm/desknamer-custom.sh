focused="$(bspc query -d focused -D)"
if grep -q "$focused" ~/.config/desknamer/desktop.blacklist; then
	desknamer -D "$focused" && notify-send "Desknamer Event" "Removed desktop $focused from blacklist"
	desknamer --quiet --rename "$focused"
else
	new="$(echo "" | dmenu -p "Customize desktop name: ")"
	if [ -n "$new" ]; then
		bspc desktop "$focused" --rename "$new"
		desknamer --quiet -d "$focused"
	fi
fi
