<div align='center'>
	<h1>dotfiles</h1><br>
</div>

## Introduction

Welcome! This repo contains my "dotfiles," which are system configuration files located inside my home directory. Be aware that many of these files are specific to my system and you shouldn't expect them to magically work by copying + pasting them into your system. My dotfiles are managed using [GNU Stow](https://www.gnu.org/software/stow), a symlink farm manager which makes dotfiles easy to maintain and install.

## System Overview

* os: `Debian (Testing)` and `MacOS`
* wm: `bspwm` and `swaywm`
* shell: `bash`
* editor: `neovim`
* terminal: `alacritty` and `foot` and `iTerm2`
* terminal font: `Source Code Pro`

## Installation

To install my dotfiles, assuming the appropriate package and `GNU Stow` is installed, simply:

```bash
git clone https://gitlab.com/jallbrit/dotfiles ~/jallbrit-dotfiles
cd ~/jallbrit-dotfiles
stow <package name, eg. vim, bash, etc.>
```

To see a list of programs available to `stow`, just run `ls`.

> Note that not all these programs come default on a Linux system. For an automated installation of most packages and dependencies, see the `bin/install-dots` folder, namely the `install-dependencies.sh` script.
