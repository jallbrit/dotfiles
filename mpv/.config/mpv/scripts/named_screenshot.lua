function named_screenshot()
    local cmd = 'echo | dmenu -p Filename:'
    local name = string.gsub(io.popen(cmd):read('*a'), '\n$', '')
    local screenshot_format = mp.get_property("screenshot_format")

    local screenshot_dir = mp.command_native({"expand_path", "~/Pictures/mpv/"}) .. mp.get_property("filename"):gsub("%..*", "") .. "/"
    io.popen("mkdir -p \"" .. screenshot_dir .."\"")

    local screenshot_path = screenshot_dir .. name .. screenshot_format
    mp.commandv('screenshot-to-file', screenshot_path)
end

mp.add_key_binding('s', 'named_ss', named_screenshot)
