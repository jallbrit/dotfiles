# fonts

I use these fonts across various parts of my system.

# figlet

Using `.bash_aliases`, calling `figlet` will automatically force `figlet` to find its fonts inside `~/.fonts/figlet`.
