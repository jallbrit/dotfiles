# X

All of my window managers run through `X11` - a powerful display server
that consolidates system information.

# DEs and WMs

There are hundreds of available window managers (WMs) out there to, well, manage your X windows. Most Desktop Environments (DEs) (e.g. GNOME, MATE, Xfce, etc.) are simply a combination of a window manager, some simple applications to make the user experience consistent. Some prefer the ease of use of a DE, but I prefer to skip the overhead bloat and instead just run a main window manager by itself.

However, there are times where having a DE is useful. If I find myself using `GNOME 3`, I use the following `GNOME`-extensions:

* `Dash to Panel`: moves the default dash to a configurable panel with autohide capabilities
* `Extensions`: provides an easy way to manage installed extensions
* `No Title Bar`: to save screen space
* `Sound Input & Output Device Chooser`

# configuration

There are many possible files that can be used in an `X` server setup, whose purposes can sometimes be confusing. My `X` setup contains only some of these possible files. To help clear things up:

* `.Xdefaults`: old method of storing `X` resources that is re-read every time an Xlib program is started.

* `.Xresources`: newer method of storing `X` resources that is loaded using `xrdb ~/.Xresources`, which immediately updates all programs with the new resource values. To better organize my `.Xresources`, I placed individual program resources inside the `.Xresources.d/` folder and my `.Xresources` simply pulls resources from those files.

## other important files

* `.xinitrc`: a very important `bash` file executed with `startx`, which is a wrapper for `xinit`. `.xinitrc` usually contains commands to start default programs like `redshift` and `powerline-daemon`, and typically, an `exec` statement at the end of `.xinitrc` will start the desired Window Manager, which is useful for Linux setups that don't use a Login/Display Manager (e.g. `lightdm` or `gdm`) and instead boot to a login console. **Important:** when `.xinitrc` stops, `X` stops. So if the last command in `.xinitrc` is to run a Window Manager, `X` will continue to run until that Window Manager is closed.

* `.xsession`: a `bash` file executed when a (modern) Login/Display Manager invokes the `custom` session type.

* `.xsessionrc`: a `bash` file executed upon GUI login from any Login/Display Manager and any session type. This file is specific to `Debian` (and `Debian` derivatives, like `Ubuntu` or `Mint`). In the case of a missing `.xinitrc`, `startx` will use `.xsessionrc` instead.

* `.xprofile`: a `bash` file executed by Login/Display Managers just before a Window Manager is started. Similar to `xsessionrc`. This file is not a popular option and quite honestly I don't know much about it.

## XDG

`XDG` is an acronym from freedesktop that is used in certain tools that manage the X environment. `.config/user-dirs.dirs` is a file that will set defaults for certain folders.
