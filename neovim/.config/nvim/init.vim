call plug#begin()

" usability
Plug 'lewis6991/gitsigns.nvim'		" git signs
Plug 'rstacruz/vim-closer'		" auto-close pairs upon hitting enter
Plug 'dhruvasagar/vim-table-mode'	" easy markdown tables
Plug 'luukvbaal/nnn.nvim'		" nnn file browser support
Plug 'mbbill/undotree'			" undo history visualizer

" LSP
Plug 'onsails/lspkind-nvim'		" adds pictograms to lsp results
Plug 'williamboman/mason.nvim'		" mason is like an LSP package manager
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'neovim/nvim-lspconfig'		" adds sensible LSP default configurations and auto-starts LSP servers based on filetype

" completion
Plug 'echasnovski/mini.completion'

" syntax
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" cosmetics
Plug 'lukas-reineke/indent-blankline.nvim'

" colorschemes
Plug 'morhetz/gruvbox'

call plug#end()

"" plugin customization

let g:pandoc#folding#fdc = 0		" disable fold level column

let g:pandoc#syntax#conceal#blacklist = ['inlinemath']	" disable inline math syntax rendering in .md (it's annoying)

" colorscheme
set termguicolors		" no for terminal colors; yes for colorscheme
set background=dark
colorscheme gruvbox

" lua stuff
lua << EOF
require("nnn").setup()
require('gitsigns').setup()
require('lspkind').init()
require('mini.completion').setup()

-- automatically enable TS highlighting when available
require('nvim-treesitter.configs').setup {
  highlight = { enable = true }
}

-- setup mason and mason-lspconfig
-- ensure some specific LSP servers are installed
require("mason").setup()
require("mason-lspconfig").setup({
	ensure_installed = {
		"gopls",	-- go
		"pyright",	-- python
		"clangd",	-- C/C++
		"tailwindcss",	-- TailwindCSS
		"denols",
	}
})
-- automatically start Mason-installed LSP servers when available
require("mason-lspconfig").setup_handlers {
	function (server_name) -- default handler (optional)
		require("lspconfig")[server_name].setup {}
	end,
}

-- tweak LSP diagnostics
vim.diagnostic.config({
	virtual_text = true,		-- virtual text is when LSP shows errors at the end of the line
	signs = true,			-- signs are the characters in the gutter to help see errors
	float = {
		border = "single",	-- add border to floating window
	},
})

vim.opt.list = true
vim.opt.listchars:append "space:⋅"
vim.opt.listchars:append "eol:↴"

vim.cmd [[highlight IndentBlanklineIndent1 guifg=#E06C75 gui=nocombine]]
vim.cmd [[highlight IndentBlanklineIndent2 guifg=#E5C07B gui=nocombine]]
vim.cmd [[highlight IndentBlanklineIndent3 guifg=#98C379 gui=nocombine]]
vim.cmd [[highlight IndentBlanklineIndent4 guifg=#56B6C2 gui=nocombine]]
vim.cmd [[highlight IndentBlanklineIndent5 guifg=#61AFEF gui=nocombine]]
vim.cmd [[highlight IndentBlanklineIndent6 guifg=#C678DD gui=nocombine]]
require "ibl".setup({
	indent = {
		char = "▏",
--		highlight = {
--			"IndentBlanklineIndent1",
--			"IndentBlanklineIndent2",
--			"IndentBlanklineIndent3",
--			"IndentBlanklineIndent4",
--			"IndentBlanklineIndent5",
--			"IndentBlanklineIndent6",
--		},
	},
})

EOF

" native LSP remaps
autocmd CursorHold * lua vim.diagnostic.open_float(nil, {focus=false})

nnoremap <silent> K <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> ca <cmd>lua vim.lsp.buf.code_action()<CR>
nnoremap <silent> [e <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <silent> ]e <cmd>lua vim.lsp.diagnostic.goto_next()<CR>

set updatetime=400	" set time for CursorHold to activate

" use <Tab> and <S-Tab> to navigate completion list
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

"" customization

set splitbelow splitright	" splits open at bottom and right

" gutters
set signcolumn=yes:1	" always draw signcolumn

" show 80 character line
" set colorcolumn=80

" tabs
set noexpandtab		" tab button inserts tabs
set tabstop=8		" tab is interpreted as this many columns
set shiftwidth=8	" how many columns is indented with << or >>
set shiftround

" autocompletion
set wildmenu		" bash-like auto-completion in commands
set wildmode=list:full
set wildignorecase	" ignore case

" search
set incsearch		" searches as you type
set hlsearch		" highlights searches

set ignorecase		" ignore case while searching
set smartcase		" unless you use a capital letter

" scrolling
set scrolloff=6		" scroll screen when within first/last 6 lines of screen

" folding
set foldenable		" enable sectioning of code into folds that can be open/closed
set foldmethod=indent	" fold based on indentions
set foldlevelstart=3	" (0-99) lower number closes more folds by default
set foldnestmax=8	" 10 nested fold max

" other
syntax enable		" enable syntax processing
set hidden		" allows modified buffers to be hidden; can switch buffers w/o saving
set autoindent		" when opening new line, keep indent as line you are on
"set visualbell		" use visual bell instead of audio when doing something wrong

" highlights (must be below `syntax enable`)
highlight Comment cterm=italic
highlight Whitespace cterm=italic gui=bold guifg=Blue ctermfg=239
highlight link IndentBlanklineChar Whitespace

" line numbering
set number relativenumber	" turn hybrid line numbers on (absolute combined with relative)

set laststatus=2	" always display statusline in all windows
set showtabline=2	" always display tabline
set showcmd		" shows what you're typing as a command
set cmdheight=1		" commandline height

"set clipboard=unnamed		" sets clipboard to * (X11 primary); ctrl+v to paste
set clipboard=unnamedplus	" sets clipboard to + (desktop clipboard); mouse middle-click to paste
set mouse=

set spell spelllang=en_us	" spellcheck
set thesaurus+=/usr/share/mythes/th_en_US_v2.dat	" set thesaurus file from package: mythes-en-us

" text wrapping
set linebreak		" intelligently wraps characters by word
set breakindent		" indents broken lines
set showbreak=↪\ 	" string to put at start of lines that have been wrapped
set formatoptions+=p	" don't break lines at single spaces that follow periods
set list listchars=nbsp:␣,tab:\▏\ ,trail:•,extends:⟩,precedes:⟨	"eol:↲  good options: |▏¦┆│⎸»

" autocomplete
set completeopt=menuone,noinsert,noselect
set shortmess+=c	" hide completion extra messages

" stop auto-indenting and filling comment lines upon newline
set formatoptions-=c formatoptions-=r formatoptions-=o

"" filetype-specific settings
autocmd FileType html,css,javascript,svelte setlocal tabstop=2 noexpandtab shiftwidth=2
autocmd FileType yaml setlocal tabstop=2 expandtab shiftwidth=2
autocmd Filetype markdown setlocal conceallevel=2

"" key mappings

" thesaurus
function! s:thesaurus()
    let s:saved_ut = &ut
    if &ut > 200 | let &ut = 200 | endif
    augroup ThesaurusAuGroup
        autocmd CursorHold,CursorHoldI <buffer>
                    \ let &ut = s:saved_ut |
                    \ set iskeyword-=32 |
                    \ autocmd! ThesaurusAuGroup
    augroup END
    return ":set iskeyword+=32\<cr>vaWovea\<c-x>\<c-t>"
endfunction

nnoremap <expr> <leader>t <SID>thesaurus()

" switch between windows with ctrl
no <C-j> <C-w>j
no <C-k> <C-w>k
no <C-l> <C-w>l
no <C-h> <C-w>h

" autocorrect last incorrectly spelled word
nnoremap <leader>z [s1z=

" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" movement up/down is by visual line instead of actual line
nnoremap j gj
nnoremap k gk

" switch buffers easily
nnoremap <leader>b :ls<CR>:b<Space>
nnoremap <PageUp> :bprevious<CR>
nnoremap <PageDown> :bnext<CR>

" switch buffers via wildmenu
set wildcharm=<C-z>
nnoremap <leader>b :buffer <C-z><S-Tab>
nnoremap <leader>B :sbuffer <C-z><S-Tab>

" open file easily
map <leader>o :e<Space>**/

nnoremap <leader>u :UndotreeToggle<CR>

nnoremap <leader>n :NnnExplorer<CR>

map <C-]> :GtagsCursor<CR>

" Open quickfix at bottom of all windows
noremap <leader>q :botright copen<cr>
" Close Quickfix
noremap <leader>Q :cclose<cr>

" disable arrow keys in normal, insert, and visual modes
" also disables two-finger scrolling on laptop trackpads
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
vnoremap <up> <nop>
vnoremap <down> <nop>
vnoremap <left> <nop>
vnoremap <right> <nop>
